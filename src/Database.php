<?php
declare(strict_types=1);

namespace Zlf\AppConstants;

/**
 * 数据库值
 */
class Database
{

    /**
     * mysql  int最小值
     */
    public const  INT_MIN = -2147483648;

    /**
     * mysql  int最大值
     */
    public const INT_MAX = 2147483647;


    /**
     * mysql  int无符号类型最小值
     */
    public const  INT_SIGNED_MIN = 0;

    /**
     * mysql  int无符号类型最大值
     */
    public const INT_SIGNED_MAX = 4294967295;


    /**
     * mysql  tinyint 最小值
     */
    public const TINYINT_MIN = -128;


    /**
     * mysql  tinyint 最大值
     */
    public const TINYINT_MAX = 127;


    /**
     * mysql  tinyint  无符号 最小值
     */
    public const TINYINT_SIGNED_MIN = 0;


    /**
     * mysql  tinyint 无符号 最大值
     */
    public const TINYINT_SIGNED_MAX = 256;
}