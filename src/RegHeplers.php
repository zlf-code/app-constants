<?php
declare(strict_types=1);

namespace Zlf\AppConstants;

/**
 * 常用正则表达库
 */
class RegHeplers
{
    #匹配一个字符串必须全部是小写
    const CODE_ALL_MIN = '/^[a-z]+$/';

    #匹配一个正整数
    const INTEGER = '/^[0-9]+$/';

    //判断一个数属否是有效的正整数
    const EFFECTIVE_INTEGER = '/^[1-9]{1}[0-9]{0,10}+$/';

    #字符串只能含有数字英文字符
    const STR_IN_CODE_OR_INT = '/^[a-zA-Z0-9]+$/';

    #检测字符中是否含有空白字符
    const IS_EMPTY_STR = '/\s/';

    #手机号码正则表达式
    const MOBILE_PHONE = '/^[12]{1}[0-9]{10}$/';

    #金额验证
    const MONEY = '/(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)/';

    #验证数据库ID是否正确
    const DB_ID = '/^[1-9]{1}[0-9]{0,9}$/';

    #小写字母
    const LOWERCASE = '/^[a-z]+$/';

    #正整数
    const POSITIVE_INTEGER = '/^((0)|([1-9]{1}[0-9]{0,10}))$/';
}